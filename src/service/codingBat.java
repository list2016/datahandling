package service;

import java.util.Locale;

public class codingBat {

    /*Given three ints, a b c, one of them is small, one is medium and one is large.
    Return true if the three values are evenly spaced, so the difference between small
    and medium is the same as the difference between medium and large.

    evenlySpaced(2, 4, 6) → true
    evenlySpaced(4, 6, 2) → true
    evenlySpaced(4, 6, 3) → false*/

    public void evenlySpaced(){

    int a = 1;
    int b = 2;
    int c = 4;

    if(a>b&a>c){
        if(a-c==c-b|a-c==c-b) System.out.println(true);
        else System.out.println(false);
    }
    else if(b>c&b>a){
        if(b-a==a-c|b-c==c-a)System.out.println(true);
        else System.out.println(false);
    }
    else{
        if(c-b==b-a|c-a==a-b) System.out.println(true);
        else System.out.println(false);
    }
}

   /* Given two strings, return true if either of the strings appears at the very end of the other string,
    ignoring upper/lower case differences (in other words, the computation should not be "case sensitive").
    Note: str.toLowerCase() returns the lowercase version of a string.

            endOther("Hiabc", "abc") → true
    endOther("AbC", "HiaBc") → true
    endOther("abc", "abXabc") → true*/

    public void endOther(){
        String a = "ab";
        String b = "ab12";
        int count1 = 0;
        int count2 = 0;

        a = a.toLowerCase(Locale.ENGLISH);
        b = b.toLowerCase(Locale.ENGLISH);

        count1 = a.indexOf(b,a.length()-b.length());
        count2 = b.indexOf(a,b.length()-a.length());

        System.out.println(count1);
        System.out.println(count2);

        if(count1 !=-1 | count2 != -1) System.out.println(true);
        else System.out.println(false);
    }

   /* Return true if the given string contains a "bob" string, but where the middle 'o' char can be any char.

    bobThere("abcbob") → true
    bobThere("b9b") → true
    bobThere("bac") → false*/

    public void bobThere(){
        String str = "bob";

        if(str.replaceAll("b.b","").equals(str)) System.out.println(false);
        else System.out.println(true);
    }



}
