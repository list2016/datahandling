package service;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Math.abs;

public class service {

    public void execute(){
        age();
        differenceDate();
        stringToDate();
        square();
        threeNumber();
        maxAndmixVAlue();
        regExpression();
        phoneNumber();
    }

    public void age(){
        LocalDateTime from = LocalDateTime.of(1999, Month.JANUARY, 22, 12, 12, 34);
        LocalDateTime to = LocalDateTime.now();
        long cba = ChronoUnit.YEARS.between(from,to);
        Duration abc = Duration.between(from,to);
        System.out.println("Мой возраст \n" +"В годах: "+ cba + "\nВ днях: "+ abc.toDays() + "\nВ часах: " + abc.toHours() + "\nВ секундах: " + abc.toSeconds());

    }

    public void differenceDate(){

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.MM.yyyy");
        LocalDate firstDate = LocalDate.parse("25.07.1921", formatter);
        LocalDate secondDate = LocalDate.parse("26.07.2001", formatter);
        long abc = ChronoUnit.DAYS.between(firstDate, secondDate);
        System.out.println(abs(abc));

    }

    public void stringToDate() {

        String str = "Friday, Aug 10, 2016 12:10:56 PM";
        SimpleDateFormat format1 = new SimpleDateFormat("EEEE, MMM d,y HH:mm:ss a", Locale.ENGLISH);
        SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date1 = format1.parse(str);
            System.out.println(date1);
            System.out.println(format2.format(date1));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String str1 = "2016-08-16T10:15:30+08:00";
        SimpleDateFormat format3 = new SimpleDateFormat("EEEE, MMM d,y HH:mm:ss a", Locale.ENGLISH);
    }

    public void square(){

        int r = 10;
        BigDecimal square = new BigDecimal(r * Math.PI);
        System.out.println(square.setScale(50));

    }

    public void threeNumber(){

        String str1 = "0.1";
        String str2 = "0.15";
        String str3 = "0.25";

        double a = Double.parseDouble(str1);
        double b = Double.parseDouble(str2);
        double c = Double.parseDouble(str3);

        System.out.println(a+b==c);

    }
    public void maxAndmixVAlue(){

        int a = 1;
        int b = 2;
        int c = 3;

        ArrayList<Integer> array = new ArrayList<>();
        array.add(a);
        array.add(b);
        array.add(c);

        System.out.println(Collections.max(array));
        System.out.println(Collections.min(array));
        
    }

    public void regExpression(){
        String text = "Ребе, Ви случайно \n" + "знаете, сколько тогда Иуда получил по нынешнему?";

        Pattern p = Pattern.compile("[а-я]+\n");
        Matcher m = p.matcher(text);
        while (m.find()){
            System.out.println(m.group());
        }
    }

    public void phoneNumber(){
        String text1 = "+7 (3412) 90-0-1";

        Pattern p1 = Pattern.compile("[0-9]*-[0-9]*-*[0-9]*");
        Matcher m1 = p1.matcher(text1);
        while (m1.find()){
            System.out.println(m1.group());
        }

    }


}

